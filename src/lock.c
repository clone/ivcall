/* $Id$ */

/*
 * This file is part of ivcall.
 *
 * ivcall is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * ivcall is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ivcall; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
#include <limits.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <pwd.h>
#include <inttypes.h>

#include "lock.h"
#include "util.h"

/* Where do lockfiles reside? */
#ifndef LOCK_DIR
#define LOCK_DIR "/var/lock"
#endif

static char* lockfile = NULL;

static const char *get_lockfile(const char *dev) {
    static char lockfile[PATH_MAX];
    snprintf(lockfile, sizeof(lockfile), "%s/LCK..%s", LOCK_DIR, basename((char*) dev));
    return lockfile;
}

static const char *get_tempfile(const char *path) {
    static char t[PATH_MAX];
    snprintf(t, sizeof(t), "%s.tmp.%lu", path, (unsigned long) getpid());
    return t;
}

int device_lock(const char *dev, const char *appname) {
    struct stat st;
    int fd;
    const char *path, *temp;
    char buf[100];
    char uidbuf[32];

    if (stat(LOCK_DIR, &st) != 0 || !S_ISDIR(st.st_mode)) {
        fprintf(stderr, "Failed to lock device, directory "LOCK_DIR" not existent.\n");
        return -1;
    }

    path = get_lockfile(dev);
    temp = get_tempfile(path);

    for (;;) {
        mode_t u;
        struct passwd* pw;
        char *username;
        
        if ((fd = open(path, O_RDONLY)) < 0) {
            if (errno != ENOENT) {
                fprintf(stderr, "Failed to open lock file: %s\n", strerror(errno));
                return -1;
            }
        }
        
        if (fd >= 0) {
            ssize_t n;

            n = loop_read(fd, buf, sizeof(buf) - 1);
            close(fd);

            if (n < 0) {
                close(fd);
                fprintf(stderr, "Failed to read from lock file: %s\n", strerror(errno));
                return -1;
            }
            
            if (n > 0) {
                pid_t pid;
                
                if (n == 4)
                    pid = (pid_t) *((uint32_t*) buf);
                else {
                    unsigned long v;
                    buf[n] = 0;
                    sscanf(buf, "%lu", &v);
                    pid = (pid_t) v;
                }
                
                if (pid > 0) {
                    if (kill(pid, 0) < 0 && errno == ESRCH) {
                        fprintf(stderr, "Lockfile is stale. Overriding it.\n");
                        /* Yes, here is a race condition */
                        unlink(path);
                    } else
                        return 1;
                }
            }
        }

        u = umask(0033);
        fd = open(temp, O_WRONLY | O_CREAT | O_EXCL, 0666);
        umask(u);
        
        if (fd < 0) {
            fprintf(stderr, "Failed to create temporary lock file: %s", strerror(errno));
            return -1;
        }

        if ((pw = getpwuid(getuid())))
            username = pw->pw_name;
        else
            snprintf(username = uidbuf, sizeof(uidbuf), "%lu", (unsigned long) getuid());

        snprintf(buf, sizeof(buf), "%10lu %s %.20s\n", (unsigned long) getpid(), appname, username);
        if (loop_write(fd, buf, strlen(buf)) < 0) {
            fprintf(stderr, "Failed to write to temporary lock file: %s", strerror(errno));
            close(fd);
            return -1;
        }

        close(fd);

        if (link(temp, path) < 0) {
            if (errno == EEXIST)
                continue;

            fprintf(stderr, "Failed to link temporary lock file: %s", strerror(errno));
        }

        unlink(temp);
        lockfile = strdup(path);

        return 0;
    }
}


/* Tries to lock the first free device, returns 0 on success */
int device_lock_first(const char *appname, char *dev, size_t l) {
    int i, found = 0;

    for (i = 2; i <= 20; i++) {
        snprintf(dev, l, "/dev/ttyI%i", i);
        
        if (!device_lock(dev, appname)) {
            found = 1;
            break;
        }
    }
    
    if (found)
        return 0;
    else {
        fprintf(stderr, "Could not find free device.\n");
        return -1;
    }
}

/* Deletes the lockfile */
void device_unlock(void) {
    if (lockfile) {
        unlink(lockfile);
        free (lockfile);
        lockfile = NULL;
    }
}
