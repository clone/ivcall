#ifndef foolockhfoo
#define foolockhfoo

/* $Id$ */

/*
 * This file is part of ivcall.
 *
 * ivcall is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * ivcall is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ivcall; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

int device_lock(const char *dev, const char*appname);
int device_lock_first(const char*appname, char *dev, size_t l);
void device_unlock(void);

#endif
