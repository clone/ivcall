#ifndef fooutilhfoo
#define fooutilhfoo

/* $Id$ */

/*
 * This file is part of ivcall.
 *
 * ivcall is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * ivcall is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ivcall; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

char *get_filename(char *f);
ssize_t loop_write(int FILEDES, const void *BUFFER, size_t SIZE);
ssize_t loop_read (int FILEDES, const void *BUFFER, size_t SIZE);

#endif
